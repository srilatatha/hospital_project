<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>
	<?php 
	echo link_tag('assets/css/bootstrap.min.css');
	?>
	
</head>
<body>

<div id="container">
	<h1>Welcome to CodeIgniter!</h1>



<div class="pull-right">
	<a href="<?php echo site_url('physician/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>ID</th>
		<th>Name</th>
		<th>Language1 Id</th>
		<th>Language2 Id</th>
		<th>Hospital Id</th>
		<th>Agency Id</th>
		<th>Location Id</th>
		<th>First Name</th>
		<th>Last Name</th>
		<th>Degree</th>
		<th>Gender</th>
		<th>Specialty</th>
		<th>Sub Specialty</th>
		<th>Address</th>
		<th>City</th>
		<th>State</th>
		<th>Zip Code</th>
		<th>Phone Number</th>
		<th>Email</th>
		<th>Pass</th>
		<th>Actions</th>
    </tr>
	<?php foreach($physicians as $p){ ?>
    <tr>
		<td><?php echo $p['id']; ?></td>
		<td><?php echo $p['name']; ?></td>
		<td><?php echo $p['language1_id']; ?></td>
		<td><?php echo $p['language2_id']; ?></td>
		<td><?php echo $p['hospital_id']; ?></td>
		<td><?php echo $p['agency_id']; ?></td>
		<td><?php echo $p['location_id']; ?></td>
		<td><?php echo $p['first_name']; ?></td>
		<td><?php echo $p['last_name']; ?></td>
		<td><?php echo $p['degree']; ?></td>
		<td><?php echo $p['gender']; ?></td>
		<td><?php echo $p['specialty']; ?></td>
		<td><?php echo $p['sub_specialty']; ?></td>
		<td><?php echo $p['address']; ?></td>
		<td><?php echo $p['city']; ?></td>
		<td><?php echo $p['state']; ?></td>
		<td><?php echo $p['zip_code']; ?></td>
		<td><?php echo $p['phone_number']; ?></td>
		<td><?php echo $p['email']; ?></td>
		<td><?php echo $p['pass']; ?></td>
	
		<td>
            <a href="<?php echo site_url('physician/edit/'.$p['id']); ?>" class="btn btn-info">Edit</a> 
            <a href="<?php echo site_url('physician/remove/'.$p['id']); ?>" class="btn btn-danger">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>

</body>
</html>