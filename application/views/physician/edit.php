
<?php echo form_open('physician/edit/'.$physician['id'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="name" class="col-md-4 control-label">Name</label>
		<div class="col-md-8">
			<input type="text" name="name" value="<?php echo ($this->input->post('name') ? $this->input->post('name') : $physician['name']); ?>" class="form-control" id="name" />
		</div>
	</div>
	<div class="form-group">
		<label for="language1_id" class="col-md-4 control-label">Language1 Id</label>
		<div class="col-md-8">
			<input type="text" name="language1_id" value="<?php echo ($this->input->post('language1_id') ? $this->input->post('language1_id') : $physician['language1_id']); ?>" class="form-control" id="language1_id" />
		</div>
	</div>
	<div class="form-group">
		<label for="language2_id" class="col-md-4 control-label">Language2 Id</label>
		<div class="col-md-8">
			<input type="text" name="language2_id" value="<?php echo ($this->input->post('language2_id') ? $this->input->post('language2_id') : $physician['language2_id']); ?>" class="form-control" id="language2_id" />
		</div>
	</div>
	<div class="form-group">
		<label for="hospital_id" class="col-md-4 control-label">Hospital Id</label>
		<div class="col-md-8">
			<input type="text" name="hospital_id" value="<?php echo ($this->input->post('hospital_id') ? $this->input->post('hospital_id') : $physician['hospital_id']); ?>" class="form-control" id="hospital_id" />
		</div>
	</div>
	<div class="form-group">
		<label for="agency_id" class="col-md-4 control-label">Agency Id</label>
		<div class="col-md-8">
			<input type="text" name="agency_id" value="<?php echo ($this->input->post('agency_id') ? $this->input->post('agency_id') : $physician['agency_id']); ?>" class="form-control" id="agency_id" />
		</div>
	</div>
	<div class="form-group">
		<label for="location_id" class="col-md-4 control-label">Location Id</label>
		<div class="col-md-8">
			<input type="text" name="location_id" value="<?php echo ($this->input->post('location_id') ? $this->input->post('location_id') : $physician['location_id']); ?>" class="form-control" id="location_id" />
		</div>
	</div>
	<div class="form-group">
		<label for="first_name" class="col-md-4 control-label">First Name</label>
		<div class="col-md-8">
			<input type="text" name="first_name" value="<?php echo ($this->input->post('first_name') ? $this->input->post('first_name') : $physician['first_name']); ?>" class="form-control" id="first_name" />
		</div>
	</div>
	<div class="form-group">
		<label for="last_name" class="col-md-4 control-label">Last Name</label>
		<div class="col-md-8">
			<input type="text" name="last_name" value="<?php echo ($this->input->post('last_name') ? $this->input->post('last_name') : $physician['last_name']); ?>" class="form-control" id="last_name" />
		</div>
	</div>
	<div class="form-group">
		<label for="degree" class="col-md-4 control-label">Degree</label>
		<div class="col-md-8">
			<input type="text" name="degree" value="<?php echo ($this->input->post('degree') ? $this->input->post('degree') : $physician['degree']); ?>" class="form-control" id="degree" />
		</div>
	</div>
	<div class="form-group">
		<label for="gender" class="col-md-4 control-label">Gender</label>
		<div class="col-md-8">
			<input type="text" name="gender" value="<?php echo ($this->input->post('gender') ? $this->input->post('gender') : $physician['gender']); ?>" class="form-control" id="gender" />
		</div>
	</div>
	<div class="form-group">
		<label for="specialty" class="col-md-4 control-label">Specialty</label>
		<div class="col-md-8">
			<input type="text" name="specialty" value="<?php echo ($this->input->post('specialty') ? $this->input->post('specialty') : $physician['specialty']); ?>" class="form-control" id="specialty" />
		</div>
	</div>
	<div class="form-group">
		<label for="sub_specialty" class="col-md-4 control-label">Sub Specialty</label>
		<div class="col-md-8">
			<input type="text" name="sub_specialty" value="<?php echo ($this->input->post('sub_specialty') ? $this->input->post('sub_specialty') : $physician['sub_specialty']); ?>" class="form-control" id="sub_specialty" />
		</div>
	</div>
	<div class="form-group">
		<label for="address" class="col-md-4 control-label">Address</label>
		<div class="col-md-8">
			<input type="text" name="address" value="<?php echo ($this->input->post('address') ? $this->input->post('address') : $physician['address']); ?>" class="form-control" id="address" />
		</div>
	</div>
	<div class="form-group">
		<label for="city" class="col-md-4 control-label">City</label>
		<div class="col-md-8">
			<input type="text" name="city" value="<?php echo ($this->input->post('city') ? $this->input->post('city') : $physician['city']); ?>" class="form-control" id="city" />
		</div>
	</div>
	<div class="form-group">
		<label for="state" class="col-md-4 control-label">State</label>
		<div class="col-md-8">
			<input type="text" name="state" value="<?php echo ($this->input->post('state') ? $this->input->post('state') : $physician['state']); ?>" class="form-control" id="state" />
		</div>
	</div>
	<div class="form-group">
		<label for="zip_code" class="col-md-4 control-label">Zip Code</label>
		<div class="col-md-8">
			<input type="text" name="zip_code" value="<?php echo ($this->input->post('zip_code') ? $this->input->post('zip_code') : $physician['zip_code']); ?>" class="form-control" id="zip_code" />
		</div>
	</div>
	<div class="form-group">
		<label for="phone_number" class="col-md-4 control-label">Phone Number</label>
		<div class="col-md-8">
			<input type="text" name="phone_number" value="<?php echo ($this->input->post('phone_number') ? $this->input->post('phone_number') : $physician['phone_number']); ?>" class="form-control" id="phone_number" />
		</div>
	</div>
	<div class="form-group">
		<label for="email" class="col-md-4 control-label">Email</label>
		<div class="col-md-8">
			<input type="text" name="email" value="<?php echo ($this->input->post('email') ? $this->input->post('email') : $physician['email']); ?>" class="form-control" id="email" />
		</div>
	</div>
	<div class="form-group">
		<label for="pass" class="col-md-4 control-label">Pass</label>
		<div class="col-md-8">
			<input type="text" name="pass" value="<?php echo ($this->input->post('pass') ? $this->input->post('pass') : $physician['pass']); ?>" class="form-control" id="pass" />
		</div>
	</div>
	<div class="form-group">
		<label for="created_at" class="col-md-4 control-label">Created At</label>
		<div class="col-md-8">
			<input type="text" name="created_at" value="<?php echo ($this->input->post('created_at') ? $this->input->post('created_at') : $physician['created_at']); ?>" class="form-control" id="created_at" />
		</div>
	</div>
	<div class="form-group">
		<label for="updated_at" class="col-md-4 control-label">Updated At</label>
		<div class="col-md-8">
			<input type="text" name="updated_at" value="<?php echo ($this->input->post('updated_at') ? $this->input->post('updated_at') : $physician['updated_at']); ?>" class="form-control" id="updated_at" />
		</div>
	</div>
	<div class="form-group">
		<label for="deleted_at" class="col-md-4 control-label">Deleted At</label>
		<div class="col-md-8">
			<input type="text" name="deleted_at" value="<?php echo ($this->input->post('deleted_at') ? $this->input->post('deleted_at') : $physician['deleted_at']); ?>" class="form-control" id="deleted_at" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>