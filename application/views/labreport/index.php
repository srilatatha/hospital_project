<div class="pull-right">
	<a href="<?php echo site_url('labreport/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>ID</th>
		<th>Hospital Id</th>
		<th>Patient Id</th>
		<th>Type Of Report</th>
		<th>Created At</th>
		<th>Updated At</th>
		<th>Deleted At</th>
		<th>Actions</th>
    </tr>
	<?php foreach($labreports as $l){ ?>
    <tr>
		<td><?php echo $l['id']; ?></td>
		<td><?php echo $l['hospital_id']; ?></td>
		<td><?php echo $l['patient_id']; ?></td>
		<td><?php echo $l['type_of_report']; ?></td>
		<td><?php echo $l['created_at']; ?></td>
		<td><?php echo $l['updated_at']; ?></td>
		<td><?php echo $l['deleted_at']; ?></td>
		<td>
            <a href="<?php echo site_url('labreport/edit/'.$l['id']); ?>" class="btn btn-info">Edit</a> 
            <a href="<?php echo site_url('labreport/remove/'.$l['id']); ?>" class="btn btn-danger">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>