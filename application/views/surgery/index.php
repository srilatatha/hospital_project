<div class="pull-right">
	<a href="<?php echo site_url('surgery/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>ID</th>
		<th>Hospital Id</th>
		<th>Physician Id</th>
		<th>Patient Id</th>
		<th>Type Of Surgery</th>
		<th>Created At</th>
		<th>Updated At</th>
		<th>Deleted At</th>
		<th>Actions</th>
    </tr>
	<?php foreach($surgeries as $s){ ?>
    <tr>
		<td><?php echo $s['id']; ?></td>
		<td><?php echo $s['hospital_id']; ?></td>
		<td><?php echo $s['physician_id']; ?></td>
		<td><?php echo $s['patient_id']; ?></td>
		<td><?php echo $s['type_of_surgery']; ?></td>
		<td><?php echo $s['created_at']; ?></td>
		<td><?php echo $s['updated_at']; ?></td>
		<td><?php echo $s['deleted_at']; ?></td>
		<td>
            <a href="<?php echo site_url('surgery/edit/'.$s['id']); ?>" class="btn btn-info">Edit</a> 
            <a href="<?php echo site_url('surgery/remove/'.$s['id']); ?>" class="btn btn-danger">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>