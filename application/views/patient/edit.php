
<?php echo form_open('patient/edit/'.$patient['id'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="name" class="col-md-4 control-label">Name</label>
		<div class="col-md-8">
			<input type="text" name="name" value="<?php echo ($this->input->post('name') ? $this->input->post('name') : $patient['name']); ?>" class="form-control" id="name" />
		</div>
	</div>
	<div class="form-group">
		<label for="hospital_id" class="col-md-4 control-label">Hospital Id</label>
		<div class="col-md-8">
			<input type="text" name="hospital_id" value="<?php echo ($this->input->post('hospital_id') ? $this->input->post('hospital_id') : $patient['hospital_id']); ?>" class="form-control" id="hospital_id" />
		</div>
	</div>
	<div class="form-group">
		<label for="language_id" class="col-md-4 control-label">Language Id</label>
		<div class="col-md-8">
			<input type="text" name="language_id" value="<?php echo ($this->input->post('language_id') ? $this->input->post('language_id') : $patient['language_id']); ?>" class="form-control" id="language_id" />
		</div>
	</div>
	<div class="form-group">
		<label for="physician_id" class="col-md-4 control-label">Physician Id</label>
		<div class="col-md-8">
			<input type="text" name="physician_id" value="<?php echo ($this->input->post('physician_id') ? $this->input->post('physician_id') : $patient['physician_id']); ?>" class="form-control" id="physician_id" />
		</div>
	</div>
	<div class="form-group">
		<label for="agency_id" class="col-md-4 control-label">Agency Id</label>
		<div class="col-md-8">
			<input type="text" name="agency_id" value="<?php echo ($this->input->post('agency_id') ? $this->input->post('agency_id') : $patient['agency_id']); ?>" class="form-control" id="agency_id" />
		</div>
	</div>
	<div class="form-group">
		<label for="first_name" class="col-md-4 control-label">First Name</label>
		<div class="col-md-8">
			<input type="text" name="first_name" value="<?php echo ($this->input->post('first_name') ? $this->input->post('first_name') : $patient['first_name']); ?>" class="form-control" id="first_name" />
		</div>
	</div>
	<div class="form-group">
		<label for="last_name" class="col-md-4 control-label">Last Name</label>
		<div class="col-md-8">
			<input type="text" name="last_name" value="<?php echo ($this->input->post('last_name') ? $this->input->post('last_name') : $patient['last_name']); ?>" class="form-control" id="last_name" />
		</div>
	</div>
	<div class="form-group">
		<label for="dob" class="col-md-4 control-label">Dob</label>
		<div class="col-md-8">
			<input type="text" name="dob" value="<?php echo ($this->input->post('dob') ? $this->input->post('dob') : $patient['dob']); ?>" class="form-control" id="dob" />
		</div>
	</div>
	<div class="form-group">
		<label for="gender" class="col-md-4 control-label">Gender</label>
		<div class="col-md-8">
			<input type="text" name="gender" value="<?php echo ($this->input->post('gender') ? $this->input->post('gender') : $patient['gender']); ?>" class="form-control" id="gender" />
		</div>
	</div>
	<div class="form-group">
		<label for="address" class="col-md-4 control-label">Address</label>
		<div class="col-md-8">
			<textarea name="address"><?php echo ($this->input->post('address') ? $this->input->post('address') : $patient['address']); ?></textarea>
		</div>
	</div>
	<div class="form-group">
		<label for="register_id" class="col-md-4 control-label">Register Id</label>
		<div class="col-md-8">
			<input type="text" name="register_id" value="<?php echo ($this->input->post('register_id') ? $this->input->post('register_id') : $patient['register_id']); ?>" class="form-control" id="register_id" />
		</div>
	</div>
	<div class="form-group">
		<label for="notes" class="col-md-4 control-label">Notes</label>
		<div class="col-md-8">
			<textarea name="notes"><?php echo ($this->input->post('notes') ? $this->input->post('notes') : $patient['notes']); ?></textarea>
		</div>
	</div>
	<div class="form-group">
		<label for="issues" class="col-md-4 control-label">Issues</label>
		<div class="col-md-8">
			<input type="text" name="issues" value="<?php echo ($this->input->post('issues') ? $this->input->post('issues') : $patient['issues']); ?>" class="form-control" id="issues" />
		</div>
	</div>
	<div class="form-group">
		<label for="lab" class="col-md-4 control-label">Lab</label>
		<div class="col-md-8">
			<input type="text" name="lab" value="<?php echo ($this->input->post('lab') ? $this->input->post('lab') : $patient['lab']); ?>" class="form-control" id="lab" />
		</div>
	</div>
	<div class="form-group">
		<label for="city" class="col-md-4 control-label">City</label>
		<div class="col-md-8">
			<input type="text" name="city" value="<?php echo ($this->input->post('city') ? $this->input->post('city') : $patient['city']); ?>" class="form-control" id="city" />
		</div>
	</div>
	<div class="form-group">
		<label for="state" class="col-md-4 control-label">State</label>
		<div class="col-md-8">
			<input type="text" name="state" value="<?php echo ($this->input->post('state') ? $this->input->post('state') : $patient['state']); ?>" class="form-control" id="state" />
		</div>
	</div>
	<div class="form-group">
		<label for="zip_code" class="col-md-4 control-label">Zip Code</label>
		<div class="col-md-8">
			<input type="text" name="zip_code" value="<?php echo ($this->input->post('zip_code') ? $this->input->post('zip_code') : $patient['zip_code']); ?>" class="form-control" id="zip_code" />
		</div>
	</div>
	<div class="form-group">
		<label for="phone_number" class="col-md-4 control-label">Phone Number</label>
		<div class="col-md-8">
			<input type="text" name="phone_number" value="<?php echo ($this->input->post('phone_number') ? $this->input->post('phone_number') : $patient['phone_number']); ?>" class="form-control" id="phone_number" />
		</div>
	</div>
	<div class="form-group">
		<label for="email" class="col-md-4 control-label">Email</label>
		<div class="col-md-8">
			<input type="text" name="email" value="<?php echo ($this->input->post('email') ? $this->input->post('email') : $patient['email']); ?>" class="form-control" id="email" />
		</div>
	</div>
	<div class="form-group">
		<label for="password" class="col-md-4 control-label">Password</label>
		<div class="col-md-8">
			<input type="text" name="password" value="<?php echo ($this->input->post('password') ? $this->input->post('password') : $patient['password']); ?>" class="form-control" id="password" />
		</div>
	</div>
	<div class="form-group">
		<label for="created_at" class="col-md-4 control-label">Created At</label>
		<div class="col-md-8">
			<input type="text" name="created_at" value="<?php echo ($this->input->post('created_at') ? $this->input->post('created_at') : $patient['created_at']); ?>" class="form-control" id="created_at" />
		</div>
	</div>
	<div class="form-group">
		<label for="updated_at" class="col-md-4 control-label">Updated At</label>
		<div class="col-md-8">
			<input type="text" name="updated_at" value="<?php echo ($this->input->post('updated_at') ? $this->input->post('updated_at') : $patient['updated_at']); ?>" class="form-control" id="updated_at" />
		</div>
	</div>
	<div class="form-group">
		<label for="deleted_at" class="col-md-4 control-label">Deleted At</label>
		<div class="col-md-8">
			<input type="text" name="deleted_at" value="<?php echo ($this->input->post('deleted_at') ? $this->input->post('deleted_at') : $patient['deleted_at']); ?>" class="form-control" id="deleted_at" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>