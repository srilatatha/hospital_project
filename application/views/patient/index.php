<div class="pull-right">
	<a href="<?php echo site_url('patient/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>ID</th>
		<th>Name</th>
		<th>Hospital Id</th>
		<th>Language Id</th>
		<th>Physician Id</th>
		<th>Agency Id</th>
		<th>First Name</th>
		<th>Last Name</th>
		<th>Dob</th>
		<th>Gender</th>
		<th>Address</th>
		<th>Register Id</th>
		<th>Notes</th>
		<th>Issues</th>
		<th>Lab</th>
		<th>City</th>
		<th>State</th>
		<th>Zip Code</th>
		<th>Phone Number</th>
		<th>Email</th>
		<th>Password</th>
		<th>Created At</th>
		<th>Updated At</th>
		<th>Deleted At</th>
		<th>Actions</th>
    </tr>
	<?php foreach($patients as $p){ ?>
    <tr>
		<td><?php echo $p['id']; ?></td>
		<td><?php echo $p['name']; ?></td>
		<td><?php echo $p['hospital_id']; ?></td>
		<td><?php echo $p['language_id']; ?></td>
		<td><?php echo $p['physician_id']; ?></td>
		<td><?php echo $p['agency_id']; ?></td>
		<td><?php echo $p['first_name']; ?></td>
		<td><?php echo $p['last_name']; ?></td>
		<td><?php echo $p['dob']; ?></td>
		<td><?php echo $p['gender']; ?></td>
		<td><?php echo $p['address']; ?></td>
		<td><?php echo $p['register_id']; ?></td>
		<td><?php echo $p['notes']; ?></td>
		<td><?php echo $p['issues']; ?></td>
		<td><?php echo $p['lab']; ?></td>
		<td><?php echo $p['city']; ?></td>
		<td><?php echo $p['state']; ?></td>
		<td><?php echo $p['zip_code']; ?></td>
		<td><?php echo $p['phone_number']; ?></td>
		<td><?php echo $p['email']; ?></td>
		<td><?php echo $p['password']; ?></td>
		<td><?php echo $p['created_at']; ?></td>
		<td><?php echo $p['updated_at']; ?></td>
		<td><?php echo $p['deleted_at']; ?></td>
		<td>
            <a href="<?php echo site_url('patient/edit/'.$p['id']); ?>" class="btn btn-info">Edit</a> 
            <a href="<?php echo site_url('patient/remove/'.$p['id']); ?>" class="btn btn-danger">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>