<div class="pull-right">
	<a href="<?php echo site_url('inpatient/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>ID</th>
		<th>Hospital Id</th>
		<th>Patient Id</th>
		<th>Bed Id</th>
		<th>Created At</th>
		<th>Updated At</th>
		<th>Deleted At</th>
		<th>Actions</th>
    </tr>
	<?php foreach($inpatients as $i){ ?>
    <tr>
		<td><?php echo $i['id']; ?></td>
		<td><?php echo $i['hospital_id']; ?></td>
		<td><?php echo $i['patient_id']; ?></td>
		<td><?php echo $i['bed_id']; ?></td>
		<td><?php echo $i['created_at']; ?></td>
		<td><?php echo $i['updated_at']; ?></td>
		<td><?php echo $i['deleted_at']; ?></td>
		<td>
            <a href="<?php echo site_url('inpatient/edit/'.$i['id']); ?>" class="btn btn-info">Edit</a> 
            <a href="<?php echo site_url('inpatient/remove/'.$i['id']); ?>" class="btn btn-danger">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>