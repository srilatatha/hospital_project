
<?php echo form_open('inpatient/edit/'.$inpatient['id'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="hospital_id" class="col-md-4 control-label">Hospital Id</label>
		<div class="col-md-8">
			<input type="text" name="hospital_id" value="<?php echo ($this->input->post('hospital_id') ? $this->input->post('hospital_id') : $inpatient['hospital_id']); ?>" class="form-control" id="hospital_id" />
		</div>
	</div>
	<div class="form-group">
		<label for="patient_id" class="col-md-4 control-label">Patient Id</label>
		<div class="col-md-8">
			<input type="text" name="patient_id" value="<?php echo ($this->input->post('patient_id') ? $this->input->post('patient_id') : $inpatient['patient_id']); ?>" class="form-control" id="patient_id" />
		</div>
	</div>
	<div class="form-group">
		<label for="bed_id" class="col-md-4 control-label">Bed Id</label>
		<div class="col-md-8">
			<input type="text" name="bed_id" value="<?php echo ($this->input->post('bed_id') ? $this->input->post('bed_id') : $inpatient['bed_id']); ?>" class="form-control" id="bed_id" />
		</div>
	</div>
	<div class="form-group">
		<label for="created_at" class="col-md-4 control-label">Created At</label>
		<div class="col-md-8">
			<input type="text" name="created_at" value="<?php echo ($this->input->post('created_at') ? $this->input->post('created_at') : $inpatient['created_at']); ?>" class="form-control" id="created_at" />
		</div>
	</div>
	<div class="form-group">
		<label for="updated_at" class="col-md-4 control-label">Updated At</label>
		<div class="col-md-8">
			<input type="text" name="updated_at" value="<?php echo ($this->input->post('updated_at') ? $this->input->post('updated_at') : $inpatient['updated_at']); ?>" class="form-control" id="updated_at" />
		</div>
	</div>
	<div class="form-group">
		<label for="deleted_at" class="col-md-4 control-label">Deleted At</label>
		<div class="col-md-8">
			<input type="text" name="deleted_at" value="<?php echo ($this->input->post('deleted_at') ? $this->input->post('deleted_at') : $inpatient['deleted_at']); ?>" class="form-control" id="deleted_at" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>