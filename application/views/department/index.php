<div class="pull-right">
	<a href="<?php echo site_url('department/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>ID</th>
		<th>Hospital Id</th>
		<th>Name</th>
		<th>Location Id</th>
		<th>Phone Number</th>
		<th>Email</th>
		<th>Created At</th>
		<th>Updated At</th>
		<th>Deleted At</th>
		<th>Actions</th>
    </tr>
	<?php foreach($departments as $d){ ?>
    <tr>
		<td><?php echo $d['id']; ?></td>
		<td><?php echo $d['hospital_id']; ?></td>
		<td><?php echo $d['name']; ?></td>
		<td><?php echo $d['location_id']; ?></td>
		<td><?php echo $d['phone_number']; ?></td>
		<td><?php echo $d['email']; ?></td>
		<td><?php echo $d['created_at']; ?></td>
		<td><?php echo $d['updated_at']; ?></td>
		<td><?php echo $d['deleted_at']; ?></td>
		<td>
            <a href="<?php echo site_url('department/edit/'.$d['id']); ?>" class="btn btn-info">Edit</a> 
            <a href="<?php echo site_url('department/remove/'.$d['id']); ?>" class="btn btn-danger">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>