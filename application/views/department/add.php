<?php echo validation_errors(); ?>
<?php echo form_open('department/add',array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="hospital_id" class="col-md-4 control-label">Hospital Id</label>
		<div class="col-md-8">
			<input type="text" name="hospital_id" value="<?php echo $this->input->post('hospital_id'); ?>" class="form-control" id="hospital_id" />
		</div>
	</div>
	<div class="form-group">
		<label for="name" class="col-md-4 control-label">Name</label>
		<div class="col-md-8">
			<input type="text" name="name" value="<?php echo $this->input->post('name'); ?>" class="form-control" id="name" />
		</div>
	</div>
	<div class="form-group">
		<label for="location_id" class="col-md-4 control-label">Location Id</label>
		<div class="col-md-8">
			<input type="text" name="location_id" value="<?php echo $this->input->post('location_id'); ?>" class="form-control" id="location_id" />
		</div>
	</div>
	<div class="form-group">
		<label for="phone_number" class="col-md-4 control-label">Phone Number</label>
		<div class="col-md-8">
			<input type="text" name="phone_number" value="<?php echo $this->input->post('phone_number'); ?>" class="form-control" id="phone_number" />
		</div>
	</div>
	<div class="form-group">
		<label for="email" class="col-md-4 control-label">Email</label>
		<div class="col-md-8">
			<input type="text" name="email" value="<?php echo $this->input->post('email'); ?>" class="form-control" id="email" />
		</div>
	</div>
	<div class="form-group">
		<label for="created_at" class="col-md-4 control-label">Created At</label>
		<div class="col-md-8">
			<input type="text" name="created_at" value="<?php echo $this->input->post('created_at'); ?>" class="form-control" id="created_at" />
		</div>
	</div>
	<div class="form-group">
		<label for="updated_at" class="col-md-4 control-label">Updated At</label>
		<div class="col-md-8">
			<input type="text" name="updated_at" value="<?php echo $this->input->post('updated_at'); ?>" class="form-control" id="updated_at" />
		</div>
	</div>
	<div class="form-group">
		<label for="deleted_at" class="col-md-4 control-label">Deleted At</label>
		<div class="col-md-8">
			<input type="text" name="deleted_at" value="<?php echo $this->input->post('deleted_at'); ?>" class="form-control" id="deleted_at" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>

<?php echo form_close(); ?>