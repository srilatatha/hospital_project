<div class="pull-right">
	<a href="<?php echo site_url('bed/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>ID</th>
		<th>Hospital Id</th>
		<th>Bed Status</th>
		<th>Name</th>
		<th>Comments</th>
		<th>History</th>
		<th>Created At</th>
		<th>Updated At</th>
		<th>Deleted At</th>
		<th>Actions</th>
    </tr>
	<?php foreach($beds as $b){ ?>
    <tr>
		<td><?php echo $b['id']; ?></td>
		<td><?php echo $b['hospital_id']; ?></td>
		<td><?php echo $b['bed_status']; ?></td>
		<td><?php echo $b['name']; ?></td>
		<td><?php echo $b['comments']; ?></td>
		<td><?php echo $b['history']; ?></td>
		<td><?php echo $b['created_at']; ?></td>
		<td><?php echo $b['updated_at']; ?></td>
		<td><?php echo $b['deleted_at']; ?></td>
		<td>
            <a href="<?php echo site_url('bed/edit/'.$b['id']); ?>" class="btn btn-info">Edit</a> 
            <a href="<?php echo site_url('bed/remove/'.$b['id']); ?>" class="btn btn-danger">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>