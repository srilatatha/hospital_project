<?php echo validation_errors(); ?>
<?php echo form_open('bed/add',array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="hospital_id" class="col-md-4 control-label">Hospital Id</label>
		<div class="col-md-8">
			<input type="text" name="hospital_id" value="<?php echo $this->input->post('hospital_id'); ?>" class="form-control" id="hospital_id" />
		</div>
	</div>
	<div class="form-group">
		<label for="bed_status" class="col-md-4 control-label">Bed Status</label>
		<div class="col-md-8">
			<input type="text" name="bed_status" value="<?php echo $this->input->post('bed_status'); ?>" class="form-control" id="bed_status" />
		</div>
	</div>
	<div class="form-group">
		<label for="name" class="col-md-4 control-label">Name</label>
		<div class="col-md-8">
			<input type="text" name="name" value="<?php echo $this->input->post('name'); ?>" class="form-control" id="name" />
		</div>
	</div>
	<div class="form-group">
		<label for="comments" class="col-md-4 control-label">Comments</label>
		<div class="col-md-8">
			<textarea name="comments" class="form-control" id="comments"><?php echo $this->input->post('comments'); ?></textarea>
		</div>
	</div>
	<div class="form-group">
		<label for="history" class="col-md-4 control-label">History</label>
		<div class="col-md-8">
			<textarea name="history" class="form-control" id="history"><?php echo $this->input->post('history'); ?></textarea>
		</div>
	</div>
	<div class="form-group">
		<label for="created_at" class="col-md-4 control-label">Created At</label>
		<div class="col-md-8">
			<input type="text" name="created_at" value="<?php echo $this->input->post('created_at'); ?>" class="form-control" id="created_at" />
		</div>
	</div>
	<div class="form-group">
		<label for="updated_at" class="col-md-4 control-label">Updated At</label>
		<div class="col-md-8">
			<input type="text" name="updated_at" value="<?php echo $this->input->post('updated_at'); ?>" class="form-control" id="updated_at" />
		</div>
	</div>
	<div class="form-group">
		<label for="deleted_at" class="col-md-4 control-label">Deleted At</label>
		<div class="col-md-8">
			<input type="text" name="deleted_at" value="<?php echo $this->input->post('deleted_at'); ?>" class="form-control" id="deleted_at" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>

<?php echo form_close(); ?>