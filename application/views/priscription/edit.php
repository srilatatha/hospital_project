
<?php echo form_open('priscription/edit/'.$priscription['id'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="physician_id" class="col-md-4 control-label">Physician Id</label>
		<div class="col-md-8">
			<input type="text" name="physician_id" value="<?php echo ($this->input->post('physician_id') ? $this->input->post('physician_id') : $priscription['physician_id']); ?>" class="form-control" id="physician_id" />
		</div>
	</div>
	<div class="form-group">
		<label for="patient_id" class="col-md-4 control-label">Patient Id</label>
		<div class="col-md-8">
			<input type="text" name="patient_id" value="<?php echo ($this->input->post('patient_id') ? $this->input->post('patient_id') : $priscription['patient_id']); ?>" class="form-control" id="patient_id" />
		</div>
	</div>
	<div class="form-group">
		<label for="hospital_id" class="col-md-4 control-label">Hospital Id</label>
		<div class="col-md-8">
			<input type="text" name="hospital_id" value="<?php echo ($this->input->post('hospital_id') ? $this->input->post('hospital_id') : $priscription['hospital_id']); ?>" class="form-control" id="hospital_id" />
		</div>
	</div>
	<div class="form-group">
		<label for="priscription" class="col-md-4 control-label">Priscription</label>
		<div class="col-md-8">
			<textarea name="priscription"><?php echo ($this->input->post('priscription') ? $this->input->post('priscription') : $priscription['priscription']); ?></textarea>
		</div>
	</div>
	<div class="form-group">
		<label for="created_at" class="col-md-4 control-label">Created At</label>
		<div class="col-md-8">
			<input type="text" name="created_at" value="<?php echo ($this->input->post('created_at') ? $this->input->post('created_at') : $priscription['created_at']); ?>" class="form-control" id="created_at" />
		</div>
	</div>
	<div class="form-group">
		<label for="updated_at" class="col-md-4 control-label">Updated At</label>
		<div class="col-md-8">
			<input type="text" name="updated_at" value="<?php echo ($this->input->post('updated_at') ? $this->input->post('updated_at') : $priscription['updated_at']); ?>" class="form-control" id="updated_at" />
		</div>
	</div>
	<div class="form-group">
		<label for="deleted_at" class="col-md-4 control-label">Deleted At</label>
		<div class="col-md-8">
			<input type="text" name="deleted_at" value="<?php echo ($this->input->post('deleted_at') ? $this->input->post('deleted_at') : $priscription['deleted_at']); ?>" class="form-control" id="deleted_at" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>