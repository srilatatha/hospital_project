<div class="pull-right">
	<a href="<?php echo site_url('priscription/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>ID</th>
		<th>Physician Id</th>
		<th>Patient Id</th>
		<th>Hospital Id</th>
		<th>Priscription</th>
		<th>Created At</th>
		<th>Updated At</th>
		<th>Deleted At</th>
		<th>Actions</th>
    </tr>
	<?php foreach($priscriptions as $p){ ?>
    <tr>
		<td><?php echo $p['id']; ?></td>
		<td><?php echo $p['physician_id']; ?></td>
		<td><?php echo $p['patient_id']; ?></td>
		<td><?php echo $p['hospital_id']; ?></td>
		<td><?php echo $p['priscription']; ?></td>
		<td><?php echo $p['created_at']; ?></td>
		<td><?php echo $p['updated_at']; ?></td>
		<td><?php echo $p['deleted_at']; ?></td>
		<td>
            <a href="<?php echo site_url('priscription/edit/'.$p['id']); ?>" class="btn btn-info">Edit</a> 
            <a href="<?php echo site_url('priscription/remove/'.$p['id']); ?>" class="btn btn-danger">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>