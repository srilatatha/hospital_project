<div class="pull-right">
	<a href="<?php echo site_url('appointment/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>ID</th>
		<th>Physician Id</th>
		<th>Patient Id</th>
		<th>Hospital Id</th>
		<th>Appointment Date</th>
		<th>Created At</th>
		<th>Updated At</th>
		<th>Deleted At</th>
		<th>Actions</th>
    </tr>
	<?php foreach($appointments as $a){ ?>
    <tr>
		<td><?php echo $a['id']; ?></td>
		<td><?php echo $a['physician_id']; ?></td>
		<td><?php echo $a['patient_id']; ?></td>
		<td><?php echo $a['hospital_id']; ?></td>
		<td><?php echo $a['appointment_date']; ?></td>
		<td><?php echo $a['created_at']; ?></td>
		<td><?php echo $a['updated_at']; ?></td>
		<td><?php echo $a['deleted_at']; ?></td>
		<td>
            <a href="<?php echo site_url('appointment/edit/'.$a['id']); ?>" class="btn btn-info">Edit</a> 
            <a href="<?php echo site_url('appointment/remove/'.$a['id']); ?>" class="btn btn-danger">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>