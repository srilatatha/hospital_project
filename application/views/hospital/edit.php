<?php echo validation_errors(); ?>
<?php echo form_open('hospital/edit/'.$hospital['id'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="name" class="col-md-4 control-label">Name</label>
		<div class="col-md-8">
			<input type="text" name="name" value="<?php echo ($this->input->post('name') ? $this->input->post('name') : $hospital['name']); ?>" class="form-control" id="name" />
		</div>
	</div>
	<div class="form-group">
		<label for="city" class="col-md-4 control-label">City</label>
		<div class="col-md-8">
			<input type="text" name="city" value="<?php echo ($this->input->post('city') ? $this->input->post('city') : $hospital['city']); ?>" class="form-control" id="city" />
		</div>
	</div>
	<div class="form-group">
		<label for="state" class="col-md-4 control-label">State</label>
		<div class="col-md-8">
			<input type="text" name="state" value="<?php echo ($this->input->post('state') ? $this->input->post('state') : $hospital['state']); ?>" class="form-control" id="state" />
		</div>
	</div>
	<div class="form-group">
		<label for="phone_number" class="col-md-4 control-label">Phone Number</label>
		<div class="col-md-8">
			<input type="text" name="phone_number" value="<?php echo ($this->input->post('phone_number') ? $this->input->post('phone_number') : $hospital['phone_number']); ?>" class="form-control" id="phone_number" />
		</div>
	</div>
	<div class="form-group">
		<label for="logo" class="col-md-4 control-label">Logo</label>
		<div class="col-md-8">
			<input type="text" name="logo" value="<?php echo ($this->input->post('logo') ? $this->input->post('logo') : $hospital['logo']); ?>" class="form-control" id="logo" />
		</div>
	</div>
	<div class="form-group">
		<label for="email" class="col-md-4 control-label">Email</label>
		<div class="col-md-8">
			<input type="text" name="email" value="<?php echo ($this->input->post('email') ? $this->input->post('email') : $hospital['email']); ?>" class="form-control" id="email" />
		</div>
	</div>
	<div class="form-group">
		<label for="created_at" class="col-md-4 control-label">Created At</label>
		<div class="col-md-8">
			<input type="text" name="created_at" value="<?php echo ($this->input->post('created_at') ? $this->input->post('created_at') : $hospital['created_at']); ?>" class="form-control" id="created_at" />
		</div>
	</div>
	<div class="form-group">
		<label for="updated_at" class="col-md-4 control-label">Updated At</label>
		<div class="col-md-8">
			<input type="text" name="updated_at" value="<?php echo ($this->input->post('updated_at') ? $this->input->post('updated_at') : $hospital['updated_at']); ?>" class="form-control" id="updated_at" />
		</div>
	</div>
	<div class="form-group">
		<label for="deleted_at" class="col-md-4 control-label">Deleted At</label>
		<div class="col-md-8">
			<input type="text" name="deleted_at" value="<?php echo ($this->input->post('deleted_at') ? $this->input->post('deleted_at') : $hospital['deleted_at']); ?>" class="form-control" id="deleted_at" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>