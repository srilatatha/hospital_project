<div class="pull-right">
	<a href="<?php echo site_url('hospital/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>ID</th>
		<th>Name</th>
		<th>City</th>
		<th>State</th>
		<th>Phone Number</th>
		<th>Logo</th>
		<th>Email</th>
		<th>Created At</th>
		<th>Updated At</th>
		<th>Deleted At</th>
		<th>Actions</th>
    </tr>
	<?php foreach($hospitals as $h){ ?>
    <tr>
		<td><?php echo $h['id']; ?></td>
		<td><?php echo $h['name']; ?></td>
		<td><?php echo $h['city']; ?></td>
		<td><?php echo $h['state']; ?></td>
		<td><?php echo $h['phone_number']; ?></td>
		<td><?php echo $h['logo']; ?></td>
		<td><?php echo $h['email']; ?></td>
		<td><?php echo $h['created_at']; ?></td>
		<td><?php echo $h['updated_at']; ?></td>
		<td><?php echo $h['deleted_at']; ?></td>
		<td>
            <a href="<?php echo site_url('hospital/edit/'.$h['id']); ?>" class="btn btn-info">Edit</a> 
            <a href="<?php echo site_url('hospital/remove/'.$h['id']); ?>" class="btn btn-danger">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>