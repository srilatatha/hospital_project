<?php
/* 
 * Generated by CRUDigniter v2.3 Beta 
 * www.crudigniter.com
 */
 
class Labreport extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Labreport_model');
    } 

    /*
     * Listing of labreports
     */
    function index()
    {
        $data['labreports'] = $this->Labreport_model->get_all_labreports();

        $this->load->view('labreport/index',$data);
    }

    /*
     * Adding a new labreport
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'hospital_id' => $this->input->post('hospital_id'),
				'patient_id' => $this->input->post('patient_id'),
				'type_of_report' => $this->input->post('type_of_report'),
				'created_at' => $this->input->post('created_at'),
				'updated_at' => $this->input->post('updated_at'),
				'deleted_at' => $this->input->post('deleted_at'),
            );
            
            $labreport_id = $this->Labreport_model->add_labreport($params);
            redirect('labreport/index');
        }
        else
        {
            $this->load->view('labreport/add');
        }
    }  

    /*
     * Editing a labreport
     */
    function edit($id)
    {   
        // check if the labreport exists before trying to edit it
        $labreport = $this->Labreport_model->get_labreport($id);
        
        if(isset($labreport['id']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'hospital_id' => $this->input->post('hospital_id'),
					'patient_id' => $this->input->post('patient_id'),
					'type_of_report' => $this->input->post('type_of_report'),
					'created_at' => $this->input->post('created_at'),
					'updated_at' => $this->input->post('updated_at'),
					'deleted_at' => $this->input->post('deleted_at'),
                );

                $this->Labreport_model->update_labreport($id,$params);            
                redirect('labreport/index');
            }
            else
            {   
                $data['labreport'] = $this->Labreport_model->get_labreport($id);
    
                $this->load->view('labreport/edit',$data);
            }
        }
        else
            show_error('The labreport you are trying to edit does not exist.');
    } 

    /*
     * Deleting labreport
     */
    function remove($id)
    {
        $labreport = $this->Labreport_model->get_labreport($id);

        // check if the labreport exists before trying to delete it
        if(isset($labreport['id']))
        {
            $this->Labreport_model->delete_labreport($id);
            redirect('labreport/index');
        }
        else
            show_error('The labreport you are trying to delete does not exist.');
    }
    
}
