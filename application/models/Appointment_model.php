<?php
/* 
 * Generated by CRUDigniter v2.3 Beta 
 * www.crudigniter.com
 */
 
class Appointment_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get appointment by id
     */
    function get_appointment($id)
    {
        return $this->db->get_where('appointments',array('id'=>$id))->row_array();
    }
    
    /*
     * Get all appointments
     */
    function get_all_appointments()
    {
        return $this->db->get('appointments')->result_array();
    }
    
    /*
     * function to add new appointment
     */
    function add_appointment($params)
    {
        $this->db->insert('appointments',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update appointment
     */
    function update_appointment($id,$params)
    {
        $this->db->where('id',$id);
        $response = $this->db->update('appointments',$params);
        if($response)
        {
            return "appointment updated successfully";
        }
        else
        {
            return "Error occuring while updating appointment";
        }
    }
    
    /*
     * function to delete appointment
     */
    function delete_appointment($id)
    {
        $response = $this->db->delete('appointments',array('id'=>$id));
        if($response)
        {
            return "appointment deleted successfully";
        }
        else
        {
            return "Error occuring while deleting appointment";
        }
    }
}
